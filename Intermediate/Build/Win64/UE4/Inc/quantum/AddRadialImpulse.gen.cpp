// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "quantum/AddRadialImpulse.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAddRadialImpulse() {}
// Cross Module References
	QUANTUM_API UClass* Z_Construct_UClass_AAddRadialImpulse_NoRegister();
	QUANTUM_API UClass* Z_Construct_UClass_AAddRadialImpulse();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_quantum();
// End Cross Module References
	void AAddRadialImpulse::StaticRegisterNativesAAddRadialImpulse()
	{
	}
	UClass* Z_Construct_UClass_AAddRadialImpulse_NoRegister()
	{
		return AAddRadialImpulse::StaticClass();
	}
	struct Z_Construct_UClass_AAddRadialImpulse_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AAddRadialImpulse_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_quantum,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AAddRadialImpulse_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AddRadialImpulse.h" },
		{ "ModuleRelativePath", "AddRadialImpulse.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AAddRadialImpulse_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AAddRadialImpulse>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AAddRadialImpulse_Statics::ClassParams = {
		&AAddRadialImpulse::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_AAddRadialImpulse_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_AAddRadialImpulse_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AAddRadialImpulse()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AAddRadialImpulse_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AAddRadialImpulse, 1889348347);
	template<> QUANTUM_API UClass* StaticClass<AAddRadialImpulse>()
	{
		return AAddRadialImpulse::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AAddRadialImpulse(Z_Construct_UClass_AAddRadialImpulse, &AAddRadialImpulse::StaticClass, TEXT("/Script/quantum"), TEXT("AAddRadialImpulse"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AAddRadialImpulse);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
