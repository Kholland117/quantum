// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "quantumCharacter.h"
#include "quantumGameMode.generated.h"

UENUM()
enum class EGamePlayState
{
	EPlaying,
	EGameOver,
	EUnKnown
};

UCLASS(minimalapi)
class AquantumGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AquantumGameMode();

	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

	AquantumCharacter* MyCharacter;

	UFUNCTION(BlueprintPure, Category = "Health")
	EGamePlayState GetCurrentState() const;

	void SetCurrentState(EGamePlayState NewState);

private:
	EGamePlayState CurrentState;
	void HandleNewState(EGamePlayState NewState);
};



