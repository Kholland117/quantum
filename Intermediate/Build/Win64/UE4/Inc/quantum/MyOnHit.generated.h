// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FVector;
struct FHitResult;
#ifdef QUANTUM_MyOnHit_generated_h
#error "MyOnHit.generated.h already included, missing '#pragma once' in MyOnHit.h"
#endif
#define QUANTUM_MyOnHit_generated_h

#define quantum_Source_quantum_MyOnHit_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnCompHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnCompHit(Z_Param_HitComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define quantum_Source_quantum_MyOnHit_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnCompHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnCompHit(Z_Param_HitComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define quantum_Source_quantum_MyOnHit_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMyOnHit(); \
	friend struct Z_Construct_UClass_AMyOnHit_Statics; \
public: \
	DECLARE_CLASS(AMyOnHit, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/quantum"), NO_API) \
	DECLARE_SERIALIZER(AMyOnHit)


#define quantum_Source_quantum_MyOnHit_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAMyOnHit(); \
	friend struct Z_Construct_UClass_AMyOnHit_Statics; \
public: \
	DECLARE_CLASS(AMyOnHit, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/quantum"), NO_API) \
	DECLARE_SERIALIZER(AMyOnHit)


#define quantum_Source_quantum_MyOnHit_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMyOnHit(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMyOnHit) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyOnHit); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyOnHit); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyOnHit(AMyOnHit&&); \
	NO_API AMyOnHit(const AMyOnHit&); \
public:


#define quantum_Source_quantum_MyOnHit_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMyOnHit(AMyOnHit&&); \
	NO_API AMyOnHit(const AMyOnHit&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMyOnHit); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMyOnHit); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMyOnHit)


#define quantum_Source_quantum_MyOnHit_h_12_PRIVATE_PROPERTY_OFFSET
#define quantum_Source_quantum_MyOnHit_h_9_PROLOG
#define quantum_Source_quantum_MyOnHit_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	quantum_Source_quantum_MyOnHit_h_12_PRIVATE_PROPERTY_OFFSET \
	quantum_Source_quantum_MyOnHit_h_12_RPC_WRAPPERS \
	quantum_Source_quantum_MyOnHit_h_12_INCLASS \
	quantum_Source_quantum_MyOnHit_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define quantum_Source_quantum_MyOnHit_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	quantum_Source_quantum_MyOnHit_h_12_PRIVATE_PROPERTY_OFFSET \
	quantum_Source_quantum_MyOnHit_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	quantum_Source_quantum_MyOnHit_h_12_INCLASS_NO_PURE_DECLS \
	quantum_Source_quantum_MyOnHit_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> QUANTUM_API UClass* StaticClass<class AMyOnHit>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID quantum_Source_quantum_MyOnHit_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
