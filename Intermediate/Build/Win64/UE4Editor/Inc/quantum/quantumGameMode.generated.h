// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
enum class EGamePlayState : int32;
#ifdef QUANTUM_quantumGameMode_generated_h
#error "quantumGameMode.generated.h already included, missing '#pragma once' in quantumGameMode.h"
#endif
#define QUANTUM_quantumGameMode_generated_h

#define quantum_Source_quantum_quantumGameMode_h_21_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetCurrentState) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(EGamePlayState*)Z_Param__Result=P_THIS->GetCurrentState(); \
		P_NATIVE_END; \
	}


#define quantum_Source_quantum_quantumGameMode_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetCurrentState) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		*(EGamePlayState*)Z_Param__Result=P_THIS->GetCurrentState(); \
		P_NATIVE_END; \
	}


#define quantum_Source_quantum_quantumGameMode_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAquantumGameMode(); \
	friend struct Z_Construct_UClass_AquantumGameMode_Statics; \
public: \
	DECLARE_CLASS(AquantumGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/quantum"), QUANTUM_API) \
	DECLARE_SERIALIZER(AquantumGameMode)


#define quantum_Source_quantum_quantumGameMode_h_21_INCLASS \
private: \
	static void StaticRegisterNativesAquantumGameMode(); \
	friend struct Z_Construct_UClass_AquantumGameMode_Statics; \
public: \
	DECLARE_CLASS(AquantumGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/quantum"), QUANTUM_API) \
	DECLARE_SERIALIZER(AquantumGameMode)


#define quantum_Source_quantum_quantumGameMode_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	QUANTUM_API AquantumGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AquantumGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(QUANTUM_API, AquantumGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AquantumGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	QUANTUM_API AquantumGameMode(AquantumGameMode&&); \
	QUANTUM_API AquantumGameMode(const AquantumGameMode&); \
public:


#define quantum_Source_quantum_quantumGameMode_h_21_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	QUANTUM_API AquantumGameMode(AquantumGameMode&&); \
	QUANTUM_API AquantumGameMode(const AquantumGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(QUANTUM_API, AquantumGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AquantumGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AquantumGameMode)


#define quantum_Source_quantum_quantumGameMode_h_21_PRIVATE_PROPERTY_OFFSET
#define quantum_Source_quantum_quantumGameMode_h_18_PROLOG
#define quantum_Source_quantum_quantumGameMode_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	quantum_Source_quantum_quantumGameMode_h_21_PRIVATE_PROPERTY_OFFSET \
	quantum_Source_quantum_quantumGameMode_h_21_RPC_WRAPPERS \
	quantum_Source_quantum_quantumGameMode_h_21_INCLASS \
	quantum_Source_quantum_quantumGameMode_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define quantum_Source_quantum_quantumGameMode_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	quantum_Source_quantum_quantumGameMode_h_21_PRIVATE_PROPERTY_OFFSET \
	quantum_Source_quantum_quantumGameMode_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	quantum_Source_quantum_quantumGameMode_h_21_INCLASS_NO_PURE_DECLS \
	quantum_Source_quantum_quantumGameMode_h_21_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> QUANTUM_API UClass* StaticClass<class AquantumGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID quantum_Source_quantum_quantumGameMode_h


#define FOREACH_ENUM_EGAMEPLAYSTATE(op) \
	op(EGamePlayState::EPlaying) \
	op(EGamePlayState::EGameOver) \
	op(EGamePlayState::EUnKnown) 

enum class EGamePlayState;
template<> QUANTUM_API UEnum* StaticEnum<EGamePlayState>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
