// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CameraDirector.generated.h"

UCLASS()
class QUANTUM_API ACameraDirector : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACameraDirector();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	//Allows us to set the Camera in the details Panel In Editor
	UPROPERTY(EditAnywhere, Category = Camera)
	//Creates Actor
	AActor* CameraOne;

	//Allows us to set the Camera in the details Panel In Editor
	UPROPERTY(EditAnywhere, Category = Camera)
	AActor* CameraTwo;
	 
	//Set How long between Camera Changes
	float  TimeToNextCameraChange;

	UFUNCTION()
		bool isInputkeyDown(FKey Key);
};
