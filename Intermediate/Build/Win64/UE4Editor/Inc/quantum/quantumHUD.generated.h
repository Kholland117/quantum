// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef QUANTUM_quantumHUD_generated_h
#error "quantumHUD.generated.h already included, missing '#pragma once' in quantumHUD.h"
#endif
#define QUANTUM_quantumHUD_generated_h

#define quantum_Source_quantum_quantumHUD_h_12_RPC_WRAPPERS
#define quantum_Source_quantum_quantumHUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define quantum_Source_quantum_quantumHUD_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAquantumHUD(); \
	friend struct Z_Construct_UClass_AquantumHUD_Statics; \
public: \
	DECLARE_CLASS(AquantumHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/quantum"), NO_API) \
	DECLARE_SERIALIZER(AquantumHUD)


#define quantum_Source_quantum_quantumHUD_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAquantumHUD(); \
	friend struct Z_Construct_UClass_AquantumHUD_Statics; \
public: \
	DECLARE_CLASS(AquantumHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/quantum"), NO_API) \
	DECLARE_SERIALIZER(AquantumHUD)


#define quantum_Source_quantum_quantumHUD_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AquantumHUD(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AquantumHUD) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AquantumHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AquantumHUD); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AquantumHUD(AquantumHUD&&); \
	NO_API AquantumHUD(const AquantumHUD&); \
public:


#define quantum_Source_quantum_quantumHUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AquantumHUD(AquantumHUD&&); \
	NO_API AquantumHUD(const AquantumHUD&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AquantumHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AquantumHUD); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AquantumHUD)


#define quantum_Source_quantum_quantumHUD_h_12_PRIVATE_PROPERTY_OFFSET
#define quantum_Source_quantum_quantumHUD_h_9_PROLOG
#define quantum_Source_quantum_quantumHUD_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	quantum_Source_quantum_quantumHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	quantum_Source_quantum_quantumHUD_h_12_RPC_WRAPPERS \
	quantum_Source_quantum_quantumHUD_h_12_INCLASS \
	quantum_Source_quantum_quantumHUD_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define quantum_Source_quantum_quantumHUD_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	quantum_Source_quantum_quantumHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	quantum_Source_quantum_quantumHUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	quantum_Source_quantum_quantumHUD_h_12_INCLASS_NO_PURE_DECLS \
	quantum_Source_quantum_quantumHUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> QUANTUM_API UClass* StaticClass<class AquantumHUD>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID quantum_Source_quantum_quantumHUD_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
