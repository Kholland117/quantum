// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef QUANTUM_AddRadialImpulse_generated_h
#error "AddRadialImpulse.generated.h already included, missing '#pragma once' in AddRadialImpulse.h"
#endif
#define QUANTUM_AddRadialImpulse_generated_h

#define quantum_Source_quantum_AddRadialImpulse_h_12_RPC_WRAPPERS
#define quantum_Source_quantum_AddRadialImpulse_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define quantum_Source_quantum_AddRadialImpulse_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAAddRadialImpulse(); \
	friend struct Z_Construct_UClass_AAddRadialImpulse_Statics; \
public: \
	DECLARE_CLASS(AAddRadialImpulse, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/quantum"), NO_API) \
	DECLARE_SERIALIZER(AAddRadialImpulse)


#define quantum_Source_quantum_AddRadialImpulse_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAAddRadialImpulse(); \
	friend struct Z_Construct_UClass_AAddRadialImpulse_Statics; \
public: \
	DECLARE_CLASS(AAddRadialImpulse, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/quantum"), NO_API) \
	DECLARE_SERIALIZER(AAddRadialImpulse)


#define quantum_Source_quantum_AddRadialImpulse_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AAddRadialImpulse(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AAddRadialImpulse) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAddRadialImpulse); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAddRadialImpulse); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAddRadialImpulse(AAddRadialImpulse&&); \
	NO_API AAddRadialImpulse(const AAddRadialImpulse&); \
public:


#define quantum_Source_quantum_AddRadialImpulse_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAddRadialImpulse(AAddRadialImpulse&&); \
	NO_API AAddRadialImpulse(const AAddRadialImpulse&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAddRadialImpulse); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAddRadialImpulse); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AAddRadialImpulse)


#define quantum_Source_quantum_AddRadialImpulse_h_12_PRIVATE_PROPERTY_OFFSET
#define quantum_Source_quantum_AddRadialImpulse_h_9_PROLOG
#define quantum_Source_quantum_AddRadialImpulse_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	quantum_Source_quantum_AddRadialImpulse_h_12_PRIVATE_PROPERTY_OFFSET \
	quantum_Source_quantum_AddRadialImpulse_h_12_RPC_WRAPPERS \
	quantum_Source_quantum_AddRadialImpulse_h_12_INCLASS \
	quantum_Source_quantum_AddRadialImpulse_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define quantum_Source_quantum_AddRadialImpulse_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	quantum_Source_quantum_AddRadialImpulse_h_12_PRIVATE_PROPERTY_OFFSET \
	quantum_Source_quantum_AddRadialImpulse_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	quantum_Source_quantum_AddRadialImpulse_h_12_INCLASS_NO_PURE_DECLS \
	quantum_Source_quantum_AddRadialImpulse_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> QUANTUM_API UClass* StaticClass<class AAddRadialImpulse>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID quantum_Source_quantum_AddRadialImpulse_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
