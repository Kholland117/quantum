// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FVector;
struct FHitResult;
#ifdef QUANTUM_quantumProjectile_generated_h
#error "quantumProjectile.generated.h already included, missing '#pragma once' in quantumProjectile.h"
#endif
#define QUANTUM_quantumProjectile_generated_h

#define quantum_Source_quantum_quantumProjectile_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnHit(Z_Param_HitComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define quantum_Source_quantum_quantumProjectile_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnHit(Z_Param_HitComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define quantum_Source_quantum_quantumProjectile_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAquantumProjectile(); \
	friend struct Z_Construct_UClass_AquantumProjectile_Statics; \
public: \
	DECLARE_CLASS(AquantumProjectile, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/quantum"), NO_API) \
	DECLARE_SERIALIZER(AquantumProjectile) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define quantum_Source_quantum_quantumProjectile_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAquantumProjectile(); \
	friend struct Z_Construct_UClass_AquantumProjectile_Statics; \
public: \
	DECLARE_CLASS(AquantumProjectile, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/quantum"), NO_API) \
	DECLARE_SERIALIZER(AquantumProjectile) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define quantum_Source_quantum_quantumProjectile_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AquantumProjectile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AquantumProjectile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AquantumProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AquantumProjectile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AquantumProjectile(AquantumProjectile&&); \
	NO_API AquantumProjectile(const AquantumProjectile&); \
public:


#define quantum_Source_quantum_quantumProjectile_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AquantumProjectile(AquantumProjectile&&); \
	NO_API AquantumProjectile(const AquantumProjectile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AquantumProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AquantumProjectile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AquantumProjectile)


#define quantum_Source_quantum_quantumProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CollisionComp() { return STRUCT_OFFSET(AquantumProjectile, CollisionComp); } \
	FORCEINLINE static uint32 __PPO__ProjectileMovement() { return STRUCT_OFFSET(AquantumProjectile, ProjectileMovement); }


#define quantum_Source_quantum_quantumProjectile_h_9_PROLOG
#define quantum_Source_quantum_quantumProjectile_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	quantum_Source_quantum_quantumProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	quantum_Source_quantum_quantumProjectile_h_12_RPC_WRAPPERS \
	quantum_Source_quantum_quantumProjectile_h_12_INCLASS \
	quantum_Source_quantum_quantumProjectile_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define quantum_Source_quantum_quantumProjectile_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	quantum_Source_quantum_quantumProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	quantum_Source_quantum_quantumProjectile_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	quantum_Source_quantum_quantumProjectile_h_12_INCLASS_NO_PURE_DECLS \
	quantum_Source_quantum_quantumProjectile_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> QUANTUM_API UClass* StaticClass<class AquantumProjectile>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID quantum_Source_quantum_quantumProjectile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
